﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstRotate : MonoBehaviour {
	
	[SerializeField]
	private float Speed;
		
	void Update ()
	{
		transform.rotation = Quaternion.AngleAxis(Speed * Time.deltaTime, transform.up) * transform.rotation;
	}
}
