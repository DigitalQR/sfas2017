﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockRequired : MonoBehaviour {

	[SerializeField]
	private string[] RequiredKeys;
	[SerializeField]
	private GameObject Planet;
	[SerializeField]
	private GameObject LockedPlanet;



	void Start ()
	{
		foreach (string key in RequiredKeys)
			if (!PlayerPrefs.HasKey(key))
			{
				Planet.SetActive(false);
				LockedPlanet.SetActive(true);
				return;
			}

		Planet.SetActive(true);
		LockedPlanet.SetActive(false);
	}
}
