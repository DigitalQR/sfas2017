﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelResults : MonoBehaviour {

	private Canvas ParentCanvas;
	private bool CanvasActiveLastframe;

	[SerializeField]
	private Text ScoreBreakDown;
	[SerializeField]
	private Text TotalScore;
	[SerializeField]
	private Text HighScore;

	[SerializeField]
	private Text MedalText;
	[SerializeField]
	private Sprite[] Medals;
	[SerializeField]
	private Image MedalImage;


	void Start()
	{
		ParentCanvas = GetComponentInParent<Canvas>();
	}

	void Update()
	{
		if (ParentCanvas.isActiveAndEnabled)
		{
			if (!CanvasActiveLastframe)
				Redraw();

			CanvasActiveLastframe = true;
        }
		else
			CanvasActiveLastframe = false;
    }

	void Redraw()
	{
		GameManager game = GameManager.gGameManager;
		Player player = game != null ? game.mPlayer : null;


		if (player == null)
			return;


		LevelInfo level = SceneLoader.RequestedLevel;

		int total = (int)(
			player.Health * level.HealthScoreMultiplier +
			game.GetScoreTime() * level.TimeScoreMultiplier +
			player.Score * level.GemScoreMultiplier);


		ScoreBreakDown.text =
			"(Health):   " + player.Health + " x " + level.HealthScoreMultiplier + '\n' +
			"(Time):     " + (int)game.GetScoreTime() + " x " + level.TimeScoreMultiplier + '\n' +
			"(Gems):   " + player.Score + " x " + level.GemScoreMultiplier;

		TotalScore.text = total + " Points";
		SetMedal(total, level);
		CheckScore(total, level);
	}
	
	void SetMedal(int score, LevelInfo level)
	{
		if (score >= level.DiamondScore)
		{
			MedalText.text = "Diamond";
			MedalImage.enabled = true;
			MedalImage.sprite = Medals[3];
		}

		else if (score >= level.GoldScore)
		{
			MedalText.text = "Gold";
			MedalImage.enabled = true;
			MedalImage.sprite = Medals[2];
		}

		else if (score >= level.SilverScore)
		{
			MedalText.text = "Silver";
			MedalImage.enabled = true;
			MedalImage.sprite = Medals[1];
		}

		else if (score >= level.BronzeScore)
		{
			MedalText.text = "Bronze";
			MedalImage.enabled = true;
			MedalImage.sprite = Medals[0];
		}

		else
		{
			MedalText.text = "No medals";
			MedalImage.enabled = false;
		}
	}

	void CheckScore(int score, LevelInfo level)
	{
		int OldHighScore = PlayerPrefs.GetInt(level.HighscoreKey, 0);
		
		if (score > OldHighScore)
		{
			PlayerPrefs.SetInt(level.HighscoreKey, score);
			HighScore.enabled = true;
		}
		else
			HighScore.enabled = false;

		//Set level to completed, for unlocks
		if (score >= level.BronzeScore)
			PlayerPrefs.SetInt(level.CompletedKey, 1);
    }
}

