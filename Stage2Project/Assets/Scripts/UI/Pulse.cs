﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {

	[SerializeField]
	private float Speed = 1.0f;
	[SerializeField]
	private float Magnitude = 0.2f;
	[SerializeField]
	private float StartOffset;

	private Vector3 StartScale;
	private float Counter;
	
	void Start ()
	{
		StartScale = transform.localScale;
	}

	void OnEnable()
	{
		Counter = StartOffset;
    }

	void Update ()
	{
		Counter += Time.deltaTime;

		transform.localScale = StartScale * (1.0f + Magnitude * Mathf.Sin(Counter * Speed));
	}
}
