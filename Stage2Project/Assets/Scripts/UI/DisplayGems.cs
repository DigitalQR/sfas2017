﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class DisplayGems : MonoBehaviour {

	[SerializeField]
	private Text text;
	
	void Update ()
	{
		if (!GameManager.gGameManager)
			return;

		Player player = GameManager.gGameManager.mPlayer;

		if (player == null)
			return;

		try
		{
			WaveGameManager WaveManager = (WaveGameManager)GameManager.gGameManager;
			text.text = player.Score + "/" + WaveManager.AvaliableScore;
		}
		catch (System.InvalidCastException)
		{
			text.text = "x " + player.Score;
		}
	}
}
