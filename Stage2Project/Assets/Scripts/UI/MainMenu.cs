﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void ResetStats()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
        SceneManager.LoadScene("Main Menu");
	}

	public void Quit()
	{
		PlayerPrefs.Save();
		Application.Quit();
	}
}
