﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

	[SerializeField]
	private GameObject[] Hearts;
		
	void Update ()
	{
		if (!GameManager.gGameManager)
			return;

		Player player = GameManager.gGameManager.mPlayer;

		if (player == null)
			return;

		for (int i = 0; i < Hearts.Length; i++)
			Hearts[i].SetActive(i < player.Health);
	}
}
