﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetCamera : MonoBehaviour {

	public static PlanetCamera main { get; private set; }
	private Camera mCamera;

	[SerializeField, Range(0.0f, 1.0f)]
	private float LerpFactor = 0.5f;

	private float DefaultDistance;
	private float DesiredDistance;

	private Transform DesiredLocation;

	void Start ()
	{
		main = this;
		mCamera = GetComponentInChildren<Camera>();
		DefaultDistance = mCamera.transform.position.z;
		DesiredDistance = DefaultDistance;
    }
	
	void Update ()
	{
		float distance = mCamera.transform.localPosition.z * LerpFactor + DesiredDistance * (1.0f - LerpFactor);
		mCamera.transform.localPosition = new Vector3(mCamera.transform.localPosition.x, mCamera.transform.localPosition.y, distance);

		if (DesiredLocation != null)
			transform.position = transform.position * LerpFactor + DesiredLocation.position * (1.0f - LerpFactor);
		else
			transform.position = transform.position * LerpFactor + Vector3.zero * (1.0f - LerpFactor);

	}

	public void Focus(Transform transform, float distance = -100.0f)
	{
		DesiredLocation = transform;
		DesiredDistance = distance;
	}

	public void Defocus()
	{
		DesiredLocation = null;
		DesiredDistance = DefaultDistance;
	}
}
