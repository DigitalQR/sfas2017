﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class FocusableLevel : MonoBehaviour {

	public LevelInfo Info;

	void OnMouseDown()
	{
		if (!Input.GetMouseButton(0))
			return;

		PlanetCamera.main.Focus(transform);
		LevelSelector.main.Focus(this);
    }
}
