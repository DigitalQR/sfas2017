﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[System.Serializable]
public struct LevelInfo
{
	[Header("Details")]
	public string DisplayName;
	public string LevelName;
	[TextArea(3, 10)]
	public string LevelDescription;

	[Header("Multipliers")]
	public int HealthScoreMultiplier;
	public int GemScoreMultiplier;
	public int TimeScoreMultiplier;

	[Header("Medals")]
	public int BronzeScore;
	public int SilverScore;
	public int GoldScore;
	public int DiamondScore;

	public string HighscoreKey { get { return LevelName + "_hs"; } }
	public string CompletedKey { get { return LevelName + "_completed"; } }
};


public class LevelSelector : MonoBehaviour {
	
	public static LevelSelector main { get; private set; }
	
	[SerializeField]
	private GameObject LevelCanvas;
	[SerializeField]
	private GameObject SystemCanvas;
	private FocusableLevel CurrentLevel;

	[SerializeField]
	private Text Title;
	[SerializeField]
	private Text Description;
	[SerializeField]
	private Text HighScore;
	[SerializeField]
	private Image Medal;
	[SerializeField]
	private Sprite[] Medals;

	void Start ()
	{
		main = this;
		Defocus();
    }

	public void Focus(FocusableLevel level)
	{
		LevelCanvas.SetActive(true);
		SystemCanvas.SetActive(false);

		CurrentLevel = level;
		Title.text = level.Info.DisplayName;
		Description.text = level.Info.LevelDescription;

		//Set high score + medal info
		int score = PlayerPrefs.GetInt(level.Info.HighscoreKey, 0);

		if (score >= level.Info.DiamondScore)
		{
			HighScore.text = "Diamond";
			Medal.enabled = true;
			Medal.sprite = Medals[3];
		}

		else if (score >= level.Info.GoldScore)
		{
			HighScore.text = "Gold";
			Medal.enabled = true;
			Medal.sprite = Medals[2];
		}

		else if (score >= level.Info.SilverScore)
		{
			HighScore.text = "Silver";
			Medal.enabled = true;
			Medal.sprite = Medals[1];
		}

		else if (score >= level.Info.BronzeScore)
		{
			HighScore.text = "Bronze";
			Medal.enabled = true;
			Medal.sprite = Medals[0];
		}

		else
		{
			HighScore.text = "No medals";
			Medal.enabled = false;
		}

		HighScore.text += "\n" + score + " Points";
    }

	public void Defocus()
	{
		LevelCanvas.SetActive(false);
		SystemCanvas.SetActive(true);

		if(PlanetCamera.main != null)
			PlanetCamera.main.Defocus();
	}

	public void PlaySelected()
	{
		SceneLoader.RequestedLevel = CurrentLevel.Info;
		SceneLoader.LevelRequested = true;
        SceneManager.LoadScene("Load");
    }
}
