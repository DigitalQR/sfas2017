﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerText : MonoBehaviour {

	[SerializeField]
	private Text text;
	
	void Update ()
	{
		if (GameManager.gGameManager == null)
		{
			text.text = "00:00";
			return;
		}

		int time = (int)GameManager.gGameManager.GetDisplayTime();

		int minutes = time / 60;
		int seconds = time - minutes * 60;

		text.text = minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
    }
}
