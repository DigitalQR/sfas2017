﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Shepard))]
public class GameManager : MonoBehaviour
{
	public static GameManager gGameManager { get; private set; }
    public enum State { Paused, Playing }
	
	[SerializeField]
    protected Player PlayerPrefab;
    [SerializeField]
	protected Arena Arena;

	
	public Shepard mShepard { get; private set; }
	public Player mPlayer { get; private set; }
	public State mState { get; private set; }

	protected float Timer;

	public virtual void Awake()
    {
        ScreenManager.OnNewGame += ScreenManager_OnNewGame;
        ScreenManager.OnExitGame += ScreenManager_OnExitGame;
    }

	public virtual void Start()
    {
		gGameManager = this;
        Arena.Calculate();
        mState = State.Paused;

		mPlayer = Instantiate(PlayerPrefab);
		mPlayer.transform.parent = transform;
		mPlayer.enabled = false;

		mShepard = GetComponent<Shepard>();
		mPlayer.enabled = false;
	}

	public virtual void Update()
	{
		if (mState == State.Playing)
			InGameTick();
	}

	public virtual void OnDestroy()
	{
		if (gGameManager == this)
			gGameManager = null;
	}

	public virtual void BeginNewGame()
    {
		mShepard.Slaughter();
		mPlayer.transform.position = new Vector3(0.0f, Arena.Radius, 0.0f);
        mPlayer.enabled = true;
        mState = State.Playing;
		Timer = 0.0f;
    }

	public virtual void InGameTick()
	{
		Timer += Time.deltaTime;
    }

	public virtual float GetDisplayTime()
	{
		return Timer;
	}

	public virtual float GetScoreTime()
	{
		return Timer;
	}

	public virtual void EndGame()
    {
		if (mState == State.Paused)
			return;

        mPlayer.enabled = false;
        mState = State.Paused;

		ScreenManager Screen = FindObjectOfType<ScreenManager>();
		Screen.EndGame();
    }

    private void ScreenManager_OnNewGame()
	{
		if (gGameManager == this)
			BeginNewGame();
    }

    private void ScreenManager_OnExitGame()
	{
		if (gGameManager == this)
			EndGame();
    }
}
