﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shepard : MonoBehaviour {


	[System.Serializable]
	public struct SheepObject
	{
		public GroupTag.Group Tag;
		public GameObject GameObject;
		public bool FlockTogether;

		public float Speed;
		public float GroupSpeedFactor;

		public float BuddyDistance;
		public float AvoidDistance;

		[System.NonSerialized]
		public Rigidbody Body;

	};
	

	[SerializeField]
	private SheepObject[] Spawnables;
	private Dictionary<GroupTag.Group, SheepObject> GroupObjects;

	[SerializeField, Range(1,1000)]
	private int CountPerUpdate = 1;
	[SerializeField, Range(10, 1000)]
	private int MaxFlockSize = 125;
	private int CurrentIndex;
	private List<SheepObject> mFlock;


	void Start ()
	{
		GroupObjects = new Dictionary<GroupTag.Group, SheepObject>();

		//Convert array into dictionary for ease of use
		foreach (SheepObject sheep in Spawnables)
			GroupObjects.Add(sheep.Tag, sheep);

		mFlock = new List<SheepObject>();
    }
	
	void Update ()
	{
		int Count = CountPerUpdate;
		if (Count > mFlock.Count)
			Count = mFlock.Count;

		for (int i = 0; i < Count; i++)
		{
			if (++CurrentIndex >= mFlock.Count)
				CurrentIndex = 0;

			FlockTogether(mFlock[CurrentIndex]);
		}
	}

	public GameObject Spawn(GroupTag.Group Tag)
	{
		SheepObject sheep = new SheepObject();
		sheep.Tag = Tag;
        sheep.GameObject = Instantiate(GroupObjects[Tag].GameObject);
		sheep.Body = sheep.GameObject.GetComponent<Rigidbody>();
        sheep.FlockTogether = GroupObjects[Tag].FlockTogether;

		sheep.Speed = GroupObjects[Tag].Speed;
		sheep.GroupSpeedFactor = GroupObjects[Tag].GroupSpeedFactor;

		sheep.BuddyDistance = GroupObjects[Tag].BuddyDistance;
		sheep.AvoidDistance = GroupObjects[Tag].AvoidDistance;

		mFlock.Add(sheep);

		if (mFlock.Count > MaxFlockSize)
		{
			Destroy(mFlock[0].GameObject);
			mFlock.RemoveAt(0);
		}


		return sheep.GameObject;
	}

	public void Slaughter()
	{
		foreach (SheepObject sheep in mFlock)
			Destroy(sheep.GameObject);
		mFlock.Clear();
	}

	public void Slaughter(GameObject sheepObject)
	{
		foreach (SheepObject sheep in mFlock)
		{
			if (sheep.GameObject == sheepObject)
			{
				Destroy(sheepObject);
				mFlock.Remove(sheep);
                return;
			}
		}
		Debug.LogError("Cannot slaughter sheep if it is not part of the flock");
	}

	void FlockTogether(SheepObject sheep)
	{
		if (!sheep.FlockTogether)
			return;

		Vector3 align = Vector3.zero;
		Vector3 cohesion = Vector3.zero;
		Vector3 avoid = Vector3.zero;
		int alignCount = 0;
		int cohesionCount = 0;
		int avoidCount = 0;


		foreach (SheepObject mate in mFlock)
		{
			if (sheep.Tag != mate.Tag)
				continue;

			float distance = Vector3.Distance(sheep.GameObject.transform.position, mate.GameObject.transform.position);

			if (distance > sheep.BuddyDistance)
				continue;

			Rigidbody body = mate.Body;
			align += body.velocity;
			cohesion += mate.GameObject.transform.position;
			alignCount++;
			cohesionCount++;

			if (distance < sheep.AvoidDistance)
			{
				avoid += mate.GameObject.transform.position;
				avoidCount++;
            }
		}

		align /= alignCount;
		cohesion /= cohesionCount;
		avoid /= avoidCount;

		align.Normalize();
		cohesion = cohesion - sheep.GameObject.transform.position;
		cohesion.Normalize();
		avoid = sheep.GameObject.transform.position - avoid;
		avoid.Normalize();
		
		sheep.Body.AddForce((align + cohesion) * sheep.Speed * (1.0f + sheep.GroupSpeedFactor * alignCount) * Time.deltaTime);
	}
}
