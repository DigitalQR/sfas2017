﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGameManager : GameManager
{
	[System.Serializable]
	public struct Spawnable
	{
		public GroupTag.Group Tag;
		public int Weighting;
	};

	[SerializeField]
	protected Spawnable[] Spawnables;

	[SerializeField]
	protected float StartTimeBetweenSpawns;
	[SerializeField]
	protected float MinimumTimeBetweenSpawns;
	[SerializeField]
	protected float SpawnDelta;
	protected float TimeBetweenSpawns;
	protected float mNextSpawn;


	public override void Update()
	{
		base.Update();
		if (mState == State.Playing)
		{
			mNextSpawn -= Time.deltaTime;

			if (mNextSpawn <= 0.0f)
			{
				SpawnRandom();

				mNextSpawn = TimeBetweenSpawns;
				TimeBetweenSpawns -= SpawnDelta;
				if (TimeBetweenSpawns < MinimumTimeBetweenSpawns)
					TimeBetweenSpawns = MinimumTimeBetweenSpawns;
			}
		}
	}

	void SpawnRandom()
	{
		int TotalWeight = 1;
		foreach (Spawnable spawn in Spawnables)
			TotalWeight += spawn.Weighting;

		GroupTag.Group RandomTag = GroupTag.Group.Score;
		int RandomValue = Random.Range(1, TotalWeight);

		foreach (Spawnable spawn in Spawnables)
		{
			if (RandomValue <= spawn.Weighting)
			{
				RandomTag = spawn.Tag;
				break;
			}
			else
				RandomValue -= spawn.Weighting;
		}

		GameObject instance = mShepard.Spawn(RandomTag);
		GiveRandomLocation(instance);
        instance.transform.parent = transform;
	}

	void GiveRandomLocation(GameObject obj)
	{
		float r = Arena.Radius * 2.0f;
		float theta = Random.Range(0.0f, Mathf.PI * 2.0f);
		float phi = Random.Range(0.0f, Mathf.PI * 2.0f);

		obj.transform.position = new Vector3(
			r * Mathf.Sin(theta) * Mathf.Cos(phi),
			r * Mathf.Sin(theta) * Mathf.Sin(phi),
			r * Mathf.Cos(theta)
		);

		Rigidbody Body = obj.GetComponent<Rigidbody>();

		if (Body != null)
		{
			Vector3 to_arena = (Arena.Location - obj.transform.position).normalized;

			Quaternion planet_rotation = Quaternion.FromToRotation(obj.transform.up, -to_arena) * obj.transform.rotation;
			Quaternion random_angle = Quaternion.AngleAxis(Random.Range(-360.0f, 360.0f), Vector3.up);

			Body.velocity = planet_rotation * random_angle * obj.transform.forward * 300.0f;
		}
	}

	public override void BeginNewGame()
	{
		base.BeginNewGame();
		TimeBetweenSpawns = StartTimeBetweenSpawns;
		mNextSpawn = TimeBetweenSpawns;
	}
}
