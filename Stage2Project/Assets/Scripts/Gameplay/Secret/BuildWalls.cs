﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildWalls : FlockSpawn
{
	
	[SerializeField]
	protected Vector3 Axis;

	public override void Spawn(Shepard shepard)
	{
		for (int i = 0; i < Count * 10; i += 10)
		{
			GameObject obj = shepard.Spawn(Type);
			Vector3 SpawnLocaiton = Quaternion.AngleAxis(i, Axis.normalized) * Location;

			obj.transform.position = Arena.Location + SpawnLocaiton.normalized * Arena.Radius * 2.0f + Velocity.normalized * i;
		}

		/*
		if (Velocity.sqrMagnitude == 0.0f)
			Velocity = Vector3.up;

		for (int i = 0; i< Count; i++)
		{
			GameObject obj = shepard.Spawn(Type);
			obj.transform.position = Arena.Location + Location.normalized * Arena.Radius * 2.0f + Velocity.normalized * i;

			Rigidbody body = obj.GetComponent<Rigidbody>();
			if (body != null)
				body.velocity = Velocity;
        }
		*/
	}
}
