﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockSpawn : MonoBehaviour {

	public GroupTag.Group Type;
	[SerializeField]
	protected Vector3 Location;
	[SerializeField]
	protected Vector3 Velocity;
	public int Count;

	public virtual void Spawn(Shepard shepard)
	{
		if (Velocity.sqrMagnitude == 0.0f)
			Velocity = Vector3.up;

		for (int i = 0; i< Count; i++)
		{
			GameObject obj = shepard.Spawn(Type);
			obj.transform.position = Arena.Location + Location.normalized * Arena.Radius * 2.0f + Velocity.normalized * i;

			Rigidbody body = obj.GetComponent<Rigidbody>();
			if (body != null)
				body.velocity = Velocity;
        }
	}
}
