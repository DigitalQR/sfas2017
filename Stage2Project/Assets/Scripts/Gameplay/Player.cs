﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{
	[SerializeField]
	private float Acceleration;
	[SerializeField, Range(0.01f, 1000.0f)]
	private float MaxSpeed = 0.01f;

	public int Score { get; private set; }
	public int Health { get; private set; }
	[SerializeField]
	private int StartHealth = 3;

	[SerializeField]
	private float InvincibilityTime = 2;
	private float InvincibilityCounter;
	public bool IsInvincible { get { return InvincibilityCounter > 0.0f; } }

	[SerializeField]
	private GameObject mMesh;
	private Rigidbody mBody;

	void Start()
    {
        mBody = GetComponent<Rigidbody>();
    }

	void OnEnable()
	{
		InvincibilityCounter = InvincibilityTime;
		Health = StartHealth;
		Score = 0;
        mMesh.SetActive(true);
	}

    void Update()
	{
		if (Health <= 0.0f)
			return;

		UpdateMovement();
		HandleInvincibility();
    }

	private void HandleInvincibility()
	{
		InvincibilityCounter -= Time.deltaTime;

		if (InvincibilityCounter <= 0.0f)
		{
			mMesh.SetActive(true);
			return;
		}

		//Make mesh flash whilst invi
		int flashValue = (int)(InvincibilityCounter * 4) % 2;
		if (flashValue == 0)
			mMesh.SetActive(true);
		else
			mMesh.SetActive(false);
	}

	private void UpdateMovement()
	{
		Vector3 direction = 
			transform.right * Input.GetAxis("Horizontal") +
			transform.forward * Input.GetAxis("Vertical");

		mBody.AddForce(direction.normalized * Acceleration * mBody.mass * 1000.0f * Time.deltaTime);

		if (mBody.velocity.sqrMagnitude > MaxSpeed * MaxSpeed)
			mBody.velocity = Vector3.ClampMagnitude(mBody.velocity, MaxSpeed);


		if (mBody.velocity.sqrMagnitude > 1.5f)
		{
			mMesh.transform.LookAt(transform.position + mBody.velocity);
			mMesh.transform.rotation = Quaternion.AngleAxis(90, mMesh.transform.right) * mMesh.transform.rotation;
		}
	}

	public void ScorePoint()
	{
		if (GameManager.gGameManager.mState != GameManager.State.Playing)
			return;

		Score++;
    }

	public void TakeDamage()
	{
		if (GameManager.gGameManager.mState != GameManager.State.Playing)
			return;

		Health--;
		InvincibilityCounter = InvincibilityTime;

		if (Health <= 0.0f)
		{
			GameManager.gGameManager.EndGame();
            mMesh.SetActive(false);
		}
    }

	void OnCollisionEnter(Collision collision)
	{
		if (!IsInvincible && collision.gameObject.tag.Equals("Damage"))
		{
			TakeDamage();
			GameManager.gGameManager.mShepard.Slaughter(collision.gameObject);
		}
		if (collision.gameObject.tag.Equals("Score"))
		{
			ScorePoint();
            GameManager.gGameManager.mShepard.Slaughter(collision.gameObject);
		}
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag.Equals("Score"))
		{
			ScorePoint();
			GameManager.gGameManager.mShepard.Slaughter(collider.gameObject);
		}
	}
}
