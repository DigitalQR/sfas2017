﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAbovePlayer : FlockSpawn
{
	[SerializeField]
	private float Distance = 2.0f;

	public override void Spawn(Shepard shepard)
	{
		if (Velocity.sqrMagnitude == 0.0f)
			Velocity = Vector3.up;

		Vector3 PlayerUp = GameManager.gGameManager.mPlayer.transform.up;

		for (int i = 0; i < Count; i++)
		{
			GameObject obj = shepard.Spawn(Type);
			obj.transform.position = Arena.Location + PlayerUp.normalized * Arena.Radius * Distance + Velocity.normalized * i;

			Rigidbody body = obj.GetComponent<Rigidbody>();
			if (body != null)
				body.velocity = Velocity;
		}
	}
}
