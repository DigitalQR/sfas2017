﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour {

	[SerializeField]
	private float Duration = 1.0f;
	private FlockSpawn[] Spawns;
	public int AvaliableScore { get; private set; }
	public float TotalTime { get { return Duration; } }

	void Start ()
	{
		Spawns = GetComponents<FlockSpawn>();

		foreach (FlockSpawn flock in Spawns)
			if (flock.Type == GroupTag.Group.Score)
				AvaliableScore += flock.Count;
	}

	public float Spawn(GameManager Manager)
	{
		foreach (FlockSpawn flock in Spawns)
			flock.Spawn(Manager.mShepard);

		return Duration;
    }

	
}
