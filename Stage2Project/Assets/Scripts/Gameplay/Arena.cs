﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Arena : MonoBehaviour
{
	[SerializeField]
	private float ArenaGravity = 900000.0f;

	public static float Gravity { get; private set; }
    public static float Radius { get; private set; }
    public static Vector3 Location { get; private set; }

    void Update()
    {
#if UNITY_EDITOR 
        if (!Application.isPlaying)
        {
            Calculate();
        }
#endif
		Calculate();
	}

    public void Calculate()
    {
		Location = transform.position;
		Radius = transform.localScale.y/ 2.0f;
		Gravity = ArenaGravity;
	}
}
