﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupTag : MonoBehaviour
{
    public enum Group { Score, Chaser, Drifter, Static }

    [SerializeField]
    private Group GroupCode;

    public Group Affiliation { get { return GroupCode; } }
}
