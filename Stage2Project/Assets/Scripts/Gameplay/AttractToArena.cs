﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AttractToArena : MonoBehaviour
{
	private Rigidbody mBody;

	void Start()
	{
		mBody = GetComponent<Rigidbody>();
    }

	void Awake()
    {
		
	}
	
	void Update ()
    {
        Vector3 to_arena = (Arena.Location - transform.position).normalized;
		float gravitational_force = Arena.Gravity * mBody.mass * 1000.0f / (Arena.Radius * Arena.Radius);
		mBody.AddForce(to_arena * gravitational_force * Time.deltaTime);

		transform.rotation = Quaternion.FromToRotation(transform.up, -to_arena) * transform.rotation;
    }
}
