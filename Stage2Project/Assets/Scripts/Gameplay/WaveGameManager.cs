﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WaveGameManager : GameManager
{
	private float NextWaveTime;
	private int CurrentWave = 0;

	private Wave[] Waves;
	public int AvaliableScore { get; private set; }
	public float TotalTime { get; private set; }

	public override void Start()
	{
		base.Start();
		Waves = FindObjectsOfType<Wave>();
		Array.Sort(Waves, (x,y)=> x.gameObject.name.CompareTo(y.gameObject.name));
    }

	public override void InGameTick()
	{
		base.InGameTick();
		NextWaveTime -= Time.deltaTime;

		if (NextWaveTime <= 0.0f)
		{
			//Finished level
			if (CurrentWave >= Waves.Length)
			{
				EndGame();
				return;
			}

			Debug.Log(Waves[CurrentWave].gameObject.name);
			NextWaveTime = Waves[CurrentWave].Spawn(this);
			CurrentWave++;
        }
	}

	public override float GetDisplayTime()
	{
		float time = Mathf.Ceil(TotalTime - Timer);

		if (time < 0.0f)
			return 0.0f;
		else
			return time;
	}

	public override float GetScoreTime()
	{
		float score = base.GetScoreTime();

		if (score >= TotalTime)
			return TotalTime;
		else
			return score;
	}

	public override void BeginNewGame()
	{
		base.BeginNewGame();
		CurrentWave = 0;
		NextWaveTime = 0.0f;

		//Consistent spawns if random used
		UnityEngine.Random.InitState(42);
		AvaliableScore = 0;
		TotalTime = 0;

		foreach (Wave wave in Waves)
		{
			AvaliableScore += wave.AvaliableScore;
			TotalTime += wave.TotalTime;
        }
	}
}
