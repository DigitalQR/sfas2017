﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class SceneLoader : MonoBehaviour
{
	public static LevelInfo RequestedLevel;
	public static bool LevelRequested;

	[SerializeField]
	private string[] Scenes;
	[SerializeField]
	private GameObject LoadingScreen;

	private WaitForEndOfFrame mWaitForEndOfFrame;

	private float LoadingTime = 0.0f;
	public bool Loading { get; private set; }

	void Awake()
	{
		mWaitForEndOfFrame = new WaitForEndOfFrame();
        Loading = true;
		LoadingTime = 0.0f;

		if(LevelRequested)
			StartCoroutine(LoadScenes());
		else
			StartCoroutine(LoadMainMenu());
	}

	private IEnumerator LoadMainMenu()
	{
		for (int count = 0; count < Scenes.Length; count++)
		{
			AsyncOperation ao = SceneManager.LoadSceneAsync("Main Menu", LoadSceneMode.Additive);
			if (ao != null)
			{
				while (!ao.isDone)
				{
					yield return mWaitForEndOfFrame;
				}
			}
		}

		if (LevelRequested)
		{
			AsyncOperation ao = SceneManager.LoadSceneAsync(RequestedLevel.LevelName, LoadSceneMode.Additive);
			if (ao != null)
			{
				while (!ao.isDone)
				{
					yield return mWaitForEndOfFrame;
				}
			}
			LevelRequested = false;
		}

		yield return mWaitForEndOfFrame;

		Loading = false;
	}

	private IEnumerator LoadScenes()
	{
		for( int count = 0; count < Scenes.Length; count++ )
		{
            AsyncOperation ao = SceneManager.LoadSceneAsync(Scenes[count], LoadSceneMode.Additive);
			if( ao != null )
			{
				while( !ao.isDone )
				{
					yield return mWaitForEndOfFrame;
				}
			}
		}

		if (LevelRequested)
		{
			AsyncOperation ao = SceneManager.LoadSceneAsync(RequestedLevel.LevelName, LoadSceneMode.Additive);
			if (ao != null)
			{
				while (!ao.isDone)
				{
					yield return mWaitForEndOfFrame;
				}
			}
			LevelRequested = false;
		}

		yield return mWaitForEndOfFrame;

		Loading = false;
	}

	void Update()
	{
		LoadingTime += Time.deltaTime;

		//Disable loading screen after a bit of a wait
		if (LoadingScreen != null && !(LoadingTime <= 0.5f || Loading))
		{
			Destroy(LoadingScreen);
			LoadingScreen = null;
        }
	}
}
